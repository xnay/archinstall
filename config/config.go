package config

import (
	"os"

	"github.com/AlecAivazis/survey/v2"
)

type Config struct {
	MachineType  string
	UserName     string
	Password     string
	RootPassword string
}

func NewConfig() (*Config, error) {
	var cfg Config

	qs := []*survey.Question{
		{
			Name: "MachineType",
			Prompt: &survey.Select{
				Message: "Choose a machine type:",
				Options: []string{"desktop", "laptop"},
				Default: "desktop",
			},
		},
		{
			Name:      "UserName",
			Prompt:    &survey.Input{Message: "User Name:"},
			Validate:  survey.Required,
			Transform: survey.ToLower,
		},
		{
			Name:      "Password",
			Prompt:    &survey.Input{Message: "Password:"},
			Validate:  survey.Required,
			Transform: survey.ToLower,
		},

		{
			Name:      "RootPassword",
			Prompt:    &survey.Input{Message: "Root password:"},
			Validate:  survey.Required,
			Transform: survey.ToLower,
		},
	}

	err := survey.Ask(qs, &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}

func (c *Config) SetEnv() error {
	if err := os.Setenv("MACHINE_TYPE", c.MachineType); err != nil {
		return err
	}

	if err := os.Setenv("USERNAME", c.UserName); err != nil {
		return err
	}

	if err := os.Setenv("USERPW", c.Password); err != nil {
		return err
	}

	if err := os.Setenv("ROOTPW", c.RootPassword); err != nil {
		return err
	}

	return nil
}

func ShouldBegin() (bool, error) {
	begin := false
	shouldBegin := &survey.Confirm{
		Message: " Proceed with installation?",
	}

	if err := survey.AskOne(shouldBegin, &begin); err != nil {
		return false, err
	}

	return begin, nil
}
