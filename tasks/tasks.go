package tasks

import (
	"context"
	_ "embed"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/exmoria/archinstall/check"
	"github.com/exmoria/archinstall/command"
	"github.com/pkg/errors"
)

const (
	dotFileURL        = "https://gitlab.com/xnay/dotfiles/-/archive/main/dotfiles-main.tar.gz"
	dotFileTarball    = "dotfiles-main.tar.gz"
	dotFileFolderPath = "dotfiles-main/"
	yayURL            = "https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz"
	zshPath           = "/usr/bin/zsh"
)

var (
	//go:embed files/loader.conf
	loaderConf []byte

	//go:embed files/arch.conf
	archConf []byte

	//go:embed files/locale.conf
	localeConf []byte

	//go:embed files/locale.gen
	localeGen []byte

	//go:embed files/hosts.desktop
	hostsDesktop []byte

	//go:embed files/hosts.laptop
	hostsLaptop []byte

	//go:embed files/sudoers
	sudoers []byte

	//go:embed files/reflector.conf
	reflectorConf []byte

	//go:embed files/pacman.conf
	pacmanConf []byte
)

func RecursiveUnmount(ctx context.Context) {
	_ = command.Run(ctx, "umount", "-A", "--recursive", "/mnt")
}

// EnableNTP enables the NTP service, which sets the system clock.
func EnableNTP(ctx context.Context) error {
	return command.Run(ctx, "timedatectl", "set-ntp", "true")
}

// UpdateMirrorList refreshes the mirror list.
func UpdateMirrorList(ctx context.Context) error {
	if err := command.Run(
		ctx,
		"reflector",
		"--verbose",
		"--latest",
		"5",
		"--protocol",
		"https",
		"--sort",
		"rate",
		"--save",
		"/etc/pacman.d/mirrorlist",
	); err != nil {
		return err
	}

	return command.Run(ctx, "pacman", "-Sy")
}

// Pacstrap installs Arch Linux to the mounted partition.
func Pacstrap(ctx context.Context) error {
	return command.Run(
		ctx,
		"pacstrap",
		"/mnt",
		"base",
		"base-devel",
		"linux",
		"linux-firmware",
		"intel-ucode",
		"archlinux-keyring",
	)
}

func ConfigurePacman() error {
	return os.WriteFile("/etc/pacman.conf", pacmanConf, 0o644)
}

// Genfstab generates the filesystem table (fstab) for the mounted partition.
func Genfstab(ctx context.Context) (err error) {
	fstab, err := os.OpenFile("/mnt/etc/fstab", os.O_WRONLY|os.O_APPEND, 0o644)
	if err != nil {
		return err
	}
	defer fstab.Close()

	cmd := command.New("genfstab", "-U", "/mnt")
	cmd.RedirectStdout(fstab)

	err = cmd.Run(ctx)
	if err != nil {
		return err
	}

	return nil
}

// Chroot chroots into the mounted partition. This is the last step of phase
// 1. Phase 2 configuration takes place within the chroot jail.
func Chroot(ctx context.Context) error {
	thisBin := path.Base(os.Args[0])
	binPath := path.Join("/mnt", thisBin)
	srcPath := path.Join("/root", path.Base(os.Args[0]))

	err := copyFile(binPath, srcPath)
	if err != nil {
		return err
	}

	if err := command.Run(ctx, "arch-chroot", "/mnt", "/"+thisBin, "-chroot"); err != nil {
		return err
	}

	return os.Remove(binPath)
}

// copyFile copies the given src file to dst. dst and src must be file paths, not
// directories. Existing files will be overwritten.
func copyFile(dst, src string) (err error) {
	file, err := os.Stat(dst)

	// If the destination file already exists, that's ok. It will be overwritten.
	// If the destination file doesn't exist, that's ok. It will be created.
	// If it's any other kind of error, bail.
	if !os.IsNotExist(err) && !os.IsExist(err) {
		return err
	}

	if err == nil && file.IsDir() {
		return errors.Errorf("destination must be a file, not a directory: %q is a directory", dst)
	}

	outFile, err := os.OpenFile(dst, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0o755)
	if err != nil {
		return err
	}
	defer outFile.Close()

	inFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer inFile.Close()

	_, err = io.Copy(outFile, inFile)
	return err
}

// BootloaderConfig installs the systemd-boot EFI boot manager.
func BootloaderConfig(ctx context.Context) error {
	rootLabel := os.Getenv("ROOT_UUID")
	if rootLabel == "" {
		return errors.New("empty label")
	}

	b, err := check.EFIvars()
	if err != nil {
		return fmt.Errorf("checking EFIvars: %v", err)
	}

	if !b {
		return errors.New("not booted in EFI mode")
	}

	if err := command.Run(ctx, "bootctl", "--path=/boot", "install"); err != nil {
		return err
	}

	loaderConfFile, err := os.Create("/boot/loader/loader.conf")
	if err != nil {
		return err
	}
	defer loaderConfFile.Close()

	_, err = loaderConfFile.Write(loaderConf)
	if err != nil {
		return err
	}

	archConfFile, err := os.Create("/boot/loader/entries/arch.conf")
	if err != nil {
		log.Fatal(err)
	}
	defer archConfFile.Close()

	_, err = archConfFile.Write(archConf)
	if err != nil {
		return err
	}
	_, err = archConfFile.WriteString(fmt.Sprintf("%s", rootLabel))
	if err != nil {
		return err
	}

	return nil
}

func GenLocale(ctx context.Context) error {
	localeFile, err := os.Create("/etc/locale.conf")
	if err != nil {
		return err
	}
	defer localeFile.Close()

	if _, err := localeFile.Write(localeConf); err != nil {
		return err
	}

	localeGenFile, err := os.OpenFile("/etc/locale.gen", os.O_APPEND|os.O_WRONLY, 0o644)
	if err != nil {
		return err
	}
	defer localeGenFile.Close()

	if _, err := localeGenFile.Write(localeGen); err != nil {
		return err
	}

	if err := command.Run(ctx, "locale-gen"); err != nil {
		return err
	}

	return nil
}

func SetLocalTime(ctx context.Context) error {
	return command.Run(ctx, "ln", "-sf", "/usr/share/zoneinfo/Europe/Ljubljana", "/etc/localtime")
}

func SetHWClock(ctx context.Context) error {
	return command.Run(ctx, "hwclock", "--systohc")
}

func SetKeyMap(machineType string) error {
	if machineType == "laptop" {
		return os.WriteFile("/etc/vconsole.conf", []byte("KEYMAP=slovene"), 0o644)
	}
	return os.WriteFile("/etc/vconsole.conf", []byte("KEYMAP=uk"), 0o644)
}

func SetHostName(ctx context.Context, machineType string) error {
	if machineType == "laptop" {
		return command.Run(ctx, "hostnamectl", "set-hostname", "arch-laptop")
	}
	return command.Run(ctx, "hostnamectl", "set-hostname", "arch-desktop")
}

func SetupHostsFile(machineType string) error {
	if machineType == "laptop" {
		return os.WriteFile("/etc/hosts", hostsLaptop, 0o644)
	}
	return os.WriteFile("/etc/hosts", hostsDesktop, 0o644)
}

func SetPassword(ctx context.Context, username, pwd string) error {
	if username == "" {
		return errors.New("no username given")
	}

	if pwd == "" {
		return errors.New("empty password")
	}

	cmd := command.New("chpasswd", username)
	cmd.Stdin = strings.NewReader(fmt.Sprintf("%s:%s", username, pwd))

	return cmd.Run(ctx)
}

func DisablePassword(ctx context.Context, userName string) error {
	return command.Run(ctx, "passwd", "-d", userName)
}

func InstallPkgs(ctx context.Context, machineType string, packages map[string][]string) error {
	cmd := command.New("pacman", packages["base"]...)
	cmd.Stdin = strings.NewReader("1\n")
	if err := cmd.Run(ctx); err != nil {
		return err
	}

	if machineType == "desktop" {
		cmd := command.New("pacman", packages["desktop"]...)
		cmd.Stdin = strings.NewReader("1\n")
		if err := cmd.Run(ctx); err != nil {
			return err
		}
	}

	return nil
}

func UpdateSudoers() error {
	return os.WriteFile("/etc/sudoers", sudoers, 0o440)
}

func CreateUser(ctx context.Context, userName, machineType string) error {
	if err := command.Run(ctx, "groupadd", userName); err != nil {
		return err
	}

	if err := command.Run(ctx, "useradd", "-m", "-g", userName, "-G", "wheel", "-s", zshPath, userName); err != nil {
		return err
	}

	if machineType == "desktop" {
		if err := command.Run(ctx, "usermod", "-aG", "docker", userName); err != nil {
			return err
		}
	}

	return nil
}

func ConfigureReflector() error {
	return os.WriteFile("/etc/xdg/reflector/reflector.conf", reflectorConf, 0o644)
}

func SystemdServices(ctx context.Context) error {
	services := []string{
		"fstrim.timer",
		"systemd-timesyncd.service",
		"reflector.timer",
		"NetworkManager.service",
		"systemd-boot-update.service",
		"thermald.service",
		"paccache.timer",
		"sddm.service",
	}

	for _, service := range services {
		if err := command.Run(ctx, "systemctl", "enable", service); err != nil {
			return err
		}
	}

	return nil
}

func ConfigureDotFiles(ctx context.Context, userName string) error {
	if err := os.Chdir("/tmp"); err != nil {
		return err
	}

	if err := command.Run(ctx, "sudo", "-u", userName, "wget", dotFileURL); err != nil {
		return err
	}

	if err := command.Run(ctx, "sudo", "-u", userName, "tar", "-xzf", dotFileTarball); err != nil {
		return err
	}

	homeDir := filepath.Join("/home", userName)
	return command.Run(ctx, "sudo", "-u", userName, "rsync", "-a", dotFileFolderPath, homeDir)
}

func CreateHomeFolders(ctx context.Context, userName string) error {
	p1 := path.Join("/home", userName, "Desktop")
	if err := command.Run(ctx, "sudo", "-u", userName, "mkdir", "-p", p1); err != nil {
		return err
	}

	p2 := path.Join("/home", userName, "Downloads")
	if err := command.Run(ctx, "sudo", "-u", userName, "mkdir", "-p", p2); err != nil {
		return err
	}

	p3 := path.Join("/home", userName, "Workspace")
	return command.Run(ctx, "sudo", "-u", userName, "mkdir", "-p", p3)
}

// func SetupYay(ctx context.Context, userName string) error {
// 	homeUsername := filepath.Join("/home/", userName)

// 	if err := os.Chdir(homeUsername); err != nil {
// 		return err
// 	}

// 	if err := command.Run(ctx, "git", "clone", yayURL); err != nil {
// 		return err
// 	}

// 	if err := command.Run(ctx, "chown", "-R", userName, filepath.Join(homeUsername, "yay")); err != nil {
// 		return err
// 	}

// 	if err := command.Run(ctx, "chmod", "-R", userName, filepath.Join(homeUsername, "yay")); err != nil {
// 		return err
// 	}

// 	if err := os.Chdir("/yay"); err != nil {
// 		return err
// 	}

// 	if err := command.Run(ctx, "sudo", "-u", userName, "wget", yayURL); err != nil {
// 		return err
// 	}

// 	if err := command.Run(ctx, "sudo", "-u", userName, "tar", "-xzf", "yay.tar.gz"); err != nil {
// 		return err
// 	}

// 	if err := command.Run(ctx, "chown", "-R", userName, "/tmp/yay"); err != nil {
// 		return err
// 	}

// 	if err := command.Run(ctx, "yay/"); err != nil {
// 		return err
// 	}

// 	return command.Run(ctx, "sudo", "-u", userName, "makepkg", "--noconfirm", "-si")
// }
