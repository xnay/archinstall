# Use the offical golang image to create a binary.
# This is based on debian and sets the GOPATH to /go.
# https://hub.docker.com/_/golang
FROM --platform=${BUILDPLATFORM} golang:1.17-alpine AS build

WORKDIR /src
ENV CGO_ENABLED=0

# Copy the source code into the container.
COPY . .

ARG TARGETOS
ARG TARGETARCH

# Build the binary.
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -o /out/sysconf-${TARGETOS}-${TARGETARCH} .

FROM scratch AS bin-unix
COPY --from=build out/ /




