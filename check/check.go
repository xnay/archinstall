package check

import (
	"net"
	"os"
	"time"
)

// HasInternet checks internet connectivity.
func HasInternet() error {
	timeOut := 3 * time.Second
	if _, err := net.DialTimeout("tcp", "google.com:80", timeOut); err != nil {
		return err
	}
	return nil
}

// EFIvars makes sure the system has booted in UEFI mode and that UEFI
// variables are accessible (if the directory exists, the system is booted in UEFI mode).
func EFIvars() (bool, error) {
	_, err := os.Stat("/sys/firmware/efi/efivars")

	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}
