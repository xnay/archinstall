package main

import (
	"context"
	"fmt"
	"log"

	"github.com/exmoria/archinstall/tasks"
)

func phase2(ctx context.Context, log *log.Logger) error {
	log.Println("Configuring UEFI bootloader")
	if err := tasks.BootloaderConfig(ctx); err != nil {
		return fmt.Errorf("configuring bootloader: %v", err)
	}

	log.Println("Generating locale")
	if err := tasks.GenLocale(ctx); err != nil {
		return fmt.Errorf("generating locale: %v", err)
	}

	log.Println("Setting local time")
	if err := tasks.SetLocalTime(ctx); err != nil {
		return fmt.Errorf("setting local time: %v", err)
	}

	log.Println("Setting hardware clock")
	if err := tasks.SetHWClock(ctx); err != nil {
		return fmt.Errorf("setting hardware clock: %v", err)
	}

	log.Println("Setting keymap")
	if err := tasks.SetKeyMap(machineType); err != nil {
		return fmt.Errorf("setting keymap: %v", err)
	}

	log.Println("Setting hostname")
	if err := tasks.SetHostName(ctx, machineType); err != nil {
		return fmt.Errorf("setting hostname: %v", err)
	}

	log.Println("Setting up hosts file")
	if err := tasks.SetupHostsFile(machineType); err != nil {
		return fmt.Errorf("setting up hosts file: %v", err)
	}

	log.Println("Setting root password")
	if err := tasks.SetPassword(ctx, "root", rootPw); err != nil {
		return fmt.Errorf("setting root password: %v", err)
	}

	log.Println("Configuring pacman")
	if err := tasks.ConfigurePacman(); err != nil {
		return fmt.Errorf("configuring pacman: %v", err)
	}

	log.Println("Installing essential packages")
	if err := tasks.InstallPkgs(ctx, machineType, packages); err != nil {
		return fmt.Errorf("installing essential packages: %v", err)
	}

	log.Println("Configuring reflector")
	if err := tasks.ConfigureReflector(); err != nil {
		return fmt.Errorf("configuring reflector: %v", err)
	}

	log.Println("Updating sudoers file")
	if err := tasks.UpdateSudoers(); err != nil {
		return fmt.Errorf("updating sudoers: %v", err)
	}

	log.Printf("Creating user %s", userName)
	if err := tasks.CreateUser(ctx, userName, machineType); err != nil {
		return fmt.Errorf("creating user: %v", err)
	}

	log.Printf("Setting password for %s", userName)
	if err := tasks.SetPassword(ctx, userName, userPw); err != nil {
		return fmt.Errorf("setting password for user %s: %v", userName, err)
	}

	if machineType == "laptop" {
		log.Printf("Disabling password for user %s", userName)
		if err := tasks.DisablePassword(ctx, userName); err != nil {
			return fmt.Errorf("disabling password for user %s: %v", userName, err)
		}
	}

	log.Println("Creating home folders")
	if err := tasks.CreateHomeFolders(ctx, userName); err != nil {
		return fmt.Errorf("creating home folders: %v", err)
	}

	// log.Println("Installing AUR helper (yay)")
	// if err := tasks.SetupYay(ctx, userName); err != nil {
	// return fmt.Errorf("installing AUR helper: %v", err)
	// }

	log.Println("Configuring dotfiles")
	if err := tasks.ConfigureDotFiles(ctx, userName); err != nil {
		return fmt.Errorf("setting up dotfiles: %v", err)
	}

	log.Println("Enabling systemd services")
	if err := tasks.SystemdServices(ctx); err != nil {
		return fmt.Errorf("enabling systemd services: %v", err)
	}

	return nil
}
