package command

import (
	"context"
	"io"
	"os"
	"os/exec"
)

// Cmd represents a single shell command.
type Cmd struct {
	Path   string // path to the binary you want to Run
	Args   []string
	Stdin  io.Reader
	Stdout io.Writer
	Stderr io.Writer
	Env    []string
}

func New(name string, args ...string) *Cmd {
	return &Cmd{
		Path: name,
		Args: args,
	}
}

// Run executes the shell command.
func (c *Cmd) Run(ctx context.Context) error {
	cmd := exec.CommandContext(ctx, c.Path, c.Args...)
	cmd.Stdin = os.Stdin
	if c.Stdin != nil {
		cmd.Stdin = c.Stdin
	}

	cmd.Stdout = os.Stdout
	if c.Stdout != nil {
		cmd.Stdout = c.Stdout
	}

	cmd.Stderr = os.Stderr
	if c.Stderr != nil {
		cmd.Stderr = c.Stderr
	}

	cmd.Env = c.Env

	return cmd.Run()
}

func Run(ctx context.Context, name string, args ...string) error {
	return New(name, args...).Run(ctx)
}

// RedirectStdout will copy the stdout output from the command to the given
// writer. mycmd.RedirectStdout(myFile) is equivalent to ./mycmd > myfile.
func (c *Cmd) RedirectStdout(w io.Writer) {
	if w == nil {
		panic("passed a nil writer to sh.Cmd.RedirectStdout()")
	}

	c.Stdout = w
}

// RedirectStderr will copy the stderr output from the command to the given
// writer. mycmd.RedirectStderr(myFile) is equivalent to ./mycmd 2> myfile.
func (c *Cmd) RedirectStderr(w io.Writer) {
	if w == nil {
		panic("passed a nil writer to sh.Cmd.RedirectStderr()")
	}

	c.Stderr = w
}
