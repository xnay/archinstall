package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/exmoria/archinstall/check"
	"github.com/exmoria/archinstall/disk"
	"github.com/exmoria/archinstall/tasks"
)

func phase1(ctx context.Context, log *log.Logger, d *disk.Disk) error {
	log.Println("Checking internet connectivity")
	if err := check.HasInternet(); err != nil {
		return fmt.Errorf("no internet connectivity: %v", err)
	}

	log.Println("Checking boot mode")
	b, err := check.EFIvars()
	if err != nil {
		return fmt.Errorf("checking boot mode: %v", err)
	}
	if !b {
		return errors.New("not booted in EFI mode")
	}

	log.Println("Enabling NTP")
	if err := tasks.EnableNTP(ctx); err != nil {
		return fmt.Errorf("enabling NTP: %v", err)
	}

	log.Println("Making sure everything is unmounted before we begin")
	tasks.RecursiveUnmount(ctx)

	errc := make(chan error, 1)
	go func() {
		log.Println("Refreshing mirrors")
		errc <- tasks.UpdateMirrorList(ctx)
	}()

	log.Printf("Wiping target disk %s", d.BlockDevice)
	if err := d.Wipe(ctx); err != nil {
		return fmt.Errorf("wiping disk %s: %v", d.BlockDevice, err)
	}

	log.Printf("Partitioning target disk %s", d.BlockDevice)
	if err := d.Partition(ctx); err != nil {
		return fmt.Errorf("partitioning disk %s: %v", d.BlockDevice, err)
	}

	log.Println("Formatting partitions")
	if err := d.Format(ctx); err != nil {
		return fmt.Errorf("formatting partitions: %v", err)
	}

	log.Println("Mounting partitions")
	if err := d.Mount(ctx); err != nil {
		return fmt.Errorf("mounting partitions: %v", err)
	}

	log.Println("Setting root UUID")
	if err := d.SetRootUUID(ctx); err != nil {
		return fmt.Errorf("setting root UUID: %v", err)
	}

	log.Println("Waiting for updateMirrorList() to complete before running pacstrap")
	if err = <-errc; err != nil {
		return fmt.Errorf("waiting for updateMirrorList(): %v", err)
	}

	log.Println("Running pacstrap")
	if err := tasks.Pacstrap(ctx); err != nil {
		return fmt.Errorf("running pacstrap: %v", err)
	}

	log.Println("Generating fstab")
	if err := tasks.Genfstab(ctx); err != nil {
		return fmt.Errorf("generating fstab: %v", err)
	}

	log.Println("Phase 1 complete, entering chroot environment...")
	time.Sleep(3 * time.Second)

	return tasks.Chroot(ctx)
}
