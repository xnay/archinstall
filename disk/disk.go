package disk

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/exmoria/archinstall/command"
)

type Disk struct {
	isLaptop    bool
	BlockDevice string
	Partitions  map[string]string
}

func New(machineType string) *Disk {
	if machineType == "laptop" {
		return &Disk{
			isLaptop:    true,
			BlockDevice: "/dev/sdb",
			Partitions: map[string]string{
				"bootPart": "/dev/sdb1",
				"rootPart": "/dev/sdb2",
				"tmpPart":  "/dev/sdb3",
				"swapPart": "/dev/sdb4",
				"homePart": "/dev/sdb5",
			},
		}
	}

	return &Disk{
		BlockDevice: "/dev/nvme0n1",
		Partitions: map[string]string{
			"bootPart": "/dev/nvme0n1p1",
			"rootPart": "/dev/nvme0n1p2",
			"tmpPart":  "/dev/nvme0n1p3",
			"homePart": "/dev/nvme0n1p4",
		},
	}
}

func (d *Disk) Wipe(ctx context.Context) error {
	if err := command.Run(ctx, "sgdisk", "--zap-all", d.BlockDevice); err != nil {
		return err
	}

	// Wipe any leftover filesystem metadata.
	return command.Run(ctx, "wipefs", "--all", d.BlockDevice)
}

func (d *Disk) Partition(ctx context.Context) error {
	if err := command.Run(ctx, "sgdisk", "-og", d.BlockDevice); err != nil {
		return err
	}

	// Create an EFI partition.
	if err := command.Run(ctx, "sgdisk", "--new=0:0:+600MiB", "--change-name=0:/boot", "--typecode=0:ef00", d.BlockDevice); err != nil {
		return err
	}

	// Create a /root partition.
	if err := command.Run(ctx, "sgdisk", "--new=0:0:+35GiB", "--change-name=0:/root", "--typecode=0:8304", d.BlockDevice); err != nil {
		return err
	}

	// Create a /tmp partition.
	if err := command.Run(ctx, "sgdisk", "--new=0:0:+8GiB", "--change-name=0:/tmp", "--typecode=0:8300", d.BlockDevice); err != nil {
		return err
	}

	if d.isLaptop {
		// Create a swap partition.
		if err := command.Run(ctx, "sgdisk", "--new=0:0:+4GiB", "--change-name=0:[SWAP]", "--typecode=0:8200", d.BlockDevice); err != nil {
			return err
		}
	}

	// Create a /home partition with a relative negative end.
	offset, err := offset(ctx, d.BlockDevice)
	if err != nil {
		return err
	}

	homePart := fmt.Sprintf("--new=0:0:-%sGiB", offset)
	if err := command.Run(ctx, "sgdisk", homePart, "--change-name=0:/home", "--typecode=0:8302", d.BlockDevice); err != nil {
		return err
	}

	if err := command.Run(ctx, "partprobe", d.BlockDevice); err != nil {
		return err
	}

	return nil
}

func offset(ctx context.Context, blockDevice string) (string, error) {
	var b bytes.Buffer

	cmd := command.New("lsblk", "--output", "SIZE", "-n", "-d", blockDevice)
	cmd.Stdout = &b
	if err := cmd.Run(ctx); err != nil {
		return "", fmt.Errorf("computing disk offset: %v", err)
	}

	s := b.String()
	if strings.Contains(s, ".") {
		str := strings.Split(s, ".")[0]
		i, _ := strconv.Atoi(str)
		i = (i * 10) / 100
		return strconv.Itoa(i), nil
	}

	i, _ := strconv.Atoi(s)
	i = (i * 10) / 100
	return strconv.Itoa(i), nil
}

func (d *Disk) Format(ctx context.Context) error {
	if err := command.Run(ctx, "mkfs.fat", "-F32", d.Partitions["bootPart"]); err != nil {
		return err
	}

	if err := command.Run(ctx, "mkfs.ext4", d.Partitions["rootPart"], "-F"); err != nil {
		return err
	}

	if err := command.Run(ctx, "mkfs.ext4", d.Partitions["tmpPart"], "-F"); err != nil {
		return err
	}

	if d.isLaptop {
		if err := command.Run(ctx, "mkswap", d.Partitions["swapPart"]); err != nil {
			return err
		}
	}

	if err := command.Run(ctx, "mkfs.ext4", d.Partitions["homePart"], "-F"); err != nil {
		return err
	}

	return nil
}

func (d *Disk) Mount(ctx context.Context) error {
	if err := command.Run(ctx, "mount", d.Partitions["rootPart"], "/mnt"); err != nil {
		return err
	}

	if err := command.Run(ctx, "mkdir", "-p", "/mnt/boot"); err != nil {
		return err
	}

	if err := command.Run(ctx, "mkdir", "-p", "/mnt/tmp"); err != nil {
		return err
	}

	if err := command.Run(ctx, "mkdir", "-p", "/mnt/home"); err != nil {
		return err
	}

	if err := command.Run(ctx, "mount", d.Partitions["bootPart"], "/mnt/boot"); err != nil {
		return err
	}

	if err := command.Run(ctx, "mount", d.Partitions["tmpPart"], "/mnt/tmp"); err != nil {
		return err
	}

	if err := command.Run(ctx, "mount", d.Partitions["homePart"], "/mnt/home"); err != nil {
		return err
	}

	if d.isLaptop {
		if err := command.Run(ctx, "swapon", d.Partitions["swapPart"]); err != nil {
			return err
		}
	}

	return nil
}

func (d *Disk) SetRootUUID(ctx context.Context) error {
	var s bytes.Buffer
	cmd := command.New("lsblk", "-dno", "UUID", d.Partitions["rootPart"])
	cmd.RedirectStdout(&s)

	if err := cmd.Run(ctx); err != nil {
		return err
	}

	s.Truncate(36)

	if err := os.Setenv("ROOT_UUID", fmt.Sprintf("options root=UUID=%s rw", s.String())); err != nil {
		return err
	}

	return nil
}
