package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/exmoria/archinstall/config"
	"github.com/exmoria/archinstall/disk"
)

var (
	chroot                                = flag.Bool("chroot", false, "Pass this flag if you want to run inside arch-chroot.")
	machineType, userName, userPw, rootPw string
)

func main() {
	flag.Parse()

	log := log.New(os.Stdout, "\u001b[32m ==> \u001b[0m", log.LstdFlags|log.Ltime|log.Lshortfile)

	if err := run(context.Background(), log); err != nil {
		log.SetPrefix("\u001b[31m ==> \u001b[0m")
		log.Println("\u001b[31merror:\u001b[0m", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, log *log.Logger) error {
	var err error
	var disk disk.Disk

	if !(*chroot) {
		b, d, err := begin()
		if err != nil {
			return fmt.Errorf("starting installation: %v", err)
		}

		if !b {
			log.Println("Aborting...")
			os.Exit(0)
		}
		disk = *d
	}

	machineType = os.Getenv("MACHINE_TYPE")
	userName = os.Getenv("USERNAME")
	userPw = os.Getenv("USERPW")
	rootPw = os.Getenv("ROOTPW")

	// Installation happens in two phases. The first phase takes place on the
	// Arch USB installer. The second phase takes place in the chroot jail on
	// the mounted partition. During the second phase, this binary will be
	// copied into the chroot and run with the -chroot flag set.
	if *chroot {
		err = phase2(ctx, log)
	} else {
		err = phase1(ctx, log, &disk)
	}
	if err != nil {
		return err
	}

	if !(*chroot) {
		log.Println("Installation complete!")
		log.Println("Don't forget to umount -R /mnt before restarting...")
	}

	return nil
}

func begin() (bool, *disk.Disk, error) {
	cfg, err := config.NewConfig()
	if err != nil {
		return false, nil, err
	}

	if err := cfg.SetEnv(); err != nil {
		return false, nil, err
	}

	begin, err := config.ShouldBegin()
	if err != nil {
		return false, nil, err
	}

	return begin, disk.New(cfg.MachineType), nil
}
